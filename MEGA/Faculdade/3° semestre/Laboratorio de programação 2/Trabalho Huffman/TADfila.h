
struct lista1
{
	int quantidade;
	char valor;
	struct lista1 *prox;
};

struct fila
{
	struct lista1 *ini;
	struct lista1 *fim;
};

struct fila *criafila();
void inserefila(struct fila *f, char valor, int qtd);
int retirafila(struct fila *f);
void libera(struct fila *f);
void imprime(struct fila *f);
