#include <stdio.h>
#include <stdlib.h>
#include "h_arv.h"
#include <stdbool.h>
#include "huffman.h"
#include "TADfila.h"

void gera_arq_cod(char *arq_ent, char *arq_saida, Arv *a){
    char letra, cod[50];
    int cont = 0;
    FILE *entrada, *saida;
    entrada = fopen(arq_ent, "r");
    saida = fopen(arq_saida, "w");
    if (!entrada)   {printf("Arquivo de entrada não encontado\n");  return;}
    if (!saida)     {printf("Arquivo de saida não encontado\n");    return;}
    while ((letra = fgetc(entrada)) != EOF){
        if(gera_cod( a, letra, cod, cont) == 1) fprintf(saida, "%s", cod);
    }
    printf("Arquivo codificado gerado com sucesso\n");
    fclose(entrada);
    fclose(saida);
}

int *gera_alfa(char *arq_oco){
    FILE *ent;
    char letra;
    int tmp;
    if((ent = fopen(arq_oco, "r")) == NULL){printf("Nao foi possivel abrir o arquivo de ocorrencias\n"); exit(1);}
    int *alf = (int*) malloc(ALFABETO * sizeof(int));
    for(int i = 0; i < ALFABETO; i++){
        fscanf(ent, "%d\n", &tmp);
        alf[i] = tmp;
    }
    fclose(ent);
    return alf;
}

char busca_carac(Arv *a,FILE *ent){
    if(a->no->l != '*' && a->esq==NULL && a->dir == NULL){
        return a->no->l;
    }
    char letra = fgetc(ent);
    if(letra == EOF)    return EOF;
    if(letra == '0')    busca_carac(a->esq, ent);
    if(letra == '1')    busca_carac(a->dir, ent);
}

void desc_arq(char *arq_ent, char *arq_oco, char *arq_saida){
    FILE *entrada, *saida;
    char letra = '\0';
    int x = 0;
    entrada = fopen(arq_ent, "r");
    saida = fopen(arq_saida, "w");
    int *alf = gera_alfa(arq_oco);
    Arv *a = cria_arv_huf(alf);
    while(x == 0){
        letra = busca_carac(a, entrada);
        if(letra == EOF)    break;
        fputc(letra, saida);
    }
    printf("Arquivo descompactado com sucesso\n");
    fclose(entrada);
    fclose(saida);
}

bool marcador = true;
void buscaLargura(Arv *raiz)
{
    struct fila *f=criafila();
   if(raiz != NULL)
   {
      if (marcador == true)
      {
          inserefila(f,raiz->no->l,raiz->no->quant);
      }
       if (raiz->esq != NULL)
       {
           inserefila(f,raiz->esq->no->l,raiz->no->quant);
       }
        if(raiz->dir != NULL)
       {
           inserefila(f,raiz->dir->no->l,raiz->no->quant);
       }
        marcador = false;
        buscaLargura(raiz->esq);
        buscaLargura(raiz->dir);
   }
   imprime(f);
}