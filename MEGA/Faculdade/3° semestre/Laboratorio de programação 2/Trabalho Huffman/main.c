#include <stdio.h>
#include <stdlib.h>
#include "huffman.h"
#include "arvore.h"
#include "heap.h"
#include "comp_desc.h"
#include "TADfila.h"

int main(int argc, char ** argv){
  int i, total=0, sel, x = 0;
  enum codigo_erro erro;
  int alfabeto[ALFABETO]; /* última posição mantém o espaço */
  prioridade * cabeca=NULL; /* elemento de menor prioridade, lista encadeada */
  nodo * raiz; /* arvore com os nodos para gerar a codificacao */
  Arv *r = NULL;
  char entrada[20], oco[20], saida[20];
  while(x == 0){
    printf("******************menu****************\n");
    printf("1 - Carregar arquivo e gerar probalidades\n");
    printf("2 - Imprimir arvore usando pre-ordem\n");
    printf("3 - Imprimir arvore usando amplitude\n");
    printf("4 - Codificar Aquivo\n");
    printf("5 - Decodificar arquivo\n");
    printf("6 - Sair\n");
    printf("**************************************\n");
    while(x == 0){
      scanf("%d", &sel);
      if(sel > 0 && sel < 7)  break;
      printf("Valor incorreto\nDigite novamente\n");
    }
    switch(sel){
      case 1:
          /* primeiro parâmetro é o arquivo de entrada de prioridades, segundo é o arquivo de saída */
          if (argc!=3){
	          fprintf(stderr,"Uso: %s entradaProbabilidades saida\n",argv[0]); /* argv[0] tem o nome do executável invocado */
	          exit(1);
          }
          /* inicializa contadores com zero, para depois contar ocorrências neste vetor */
          for (i=0;i<ALFABETO;i++)  alfabeto[i] = 0;
          //PARSER DO ARQUIVO A SER CODIFICADO REALIZANDO A CONTAGEM TOTAL DE CARACTERES (A-Z-ESPAÇO) E A CONTAGEM INDIVIDUALIZADA
          conta_ocorrencias(argv[1],alfabeto,&total);
          //imprime_ocorrencias(alfabeto);
          //GERA UM ARQUIVO DE PROBABILIDADES COM OS VALORES DA CONTAGEM INDIVIDUALIZADA POR PORCENTAGEM EM RELACAO A TODOS OS CARACTERES
          gera_probabilidades(argv[2],alfabeto,total);
          /* Lê arquivo para calcular probabilidade de ocorrência de um caracter */
          if ((erro = le_ocorrencias(argv[2],alfabeto))!=ok){
	          if (erro == nao_encontrado) fprintf(stderr, "Nao pode abrir arquivo %s\n",argv[1]);
	          else if (erro == formato_incorreto) fprintf(stderr, "Formato incorreto no arquivo %s\n",argv[1]);
	          exit(1);
          }
          imprime_ocorrencias(alfabeto);
          /* Gera lista com folhas ordenadas por ordem crescente de ocorrencia */
          printf("=======lista encadeada de elementos===================\n");
          cabeca = gera_folhas(alfabeto);
          //IMPRIME A LISTA DE FOLHAS (LISTA ENCADEADA)
          imprime_conjunto_P(cabeca);
          /* Gera árvore */
          //APLICA AS TRANSFORMAÇÕES RETIRANDO DA LISTA ENCADEADA E INSERINDO NA RAIZ COMO SE FOSSE UMA ARVORE
          printf("=======gerando arvore===================\n");
          raiz = gera_arvore(cabeca);
          printf("raiz da %x\n",raiz);
          r = cria_arv_huf(alfabeto);
        break;
      case 2:
          printf("/*********************impressao em pre ordem****************************\n");
          if(r != NULL) {imp_preordem(r); printf("\n");}
          else  printf("Execute o passo 1\n"); 
        break;
      case 3:
          if(r != NULL)
          {
             buscaLargura(r);
             printf("\n");
          }
          else  printf("Execute o passo 1\n");
        break;
      case 4:
          printf("/*********************codificando o arquivo*****************************\n");
          if(r != NULL) gera_arq_cod(argv[1], "saida_cod.txt" , r);
          else  printf("Execute o passo 1\n");
        break;
      case 5:
        printf("Digite os nomes do arquivo de entrada, arquivo de ocorrencias e arquivo de saida\n");
        scanf("%s %s %s",entrada, oco, saida);
        desc_arq(entrada, oco, saida);
        break;
      case 6:
          exit(1);
        break;
    }
  }
  return 0;
}
