#include <stdio.h>
#include <stdlib.h>
#include "heap.h"


Arv *cria_arv(Arv *e, Arv *d, No *n){
    Arv *a = (Arv*) malloc(sizeof(Arv));
    a->dir = d;
    a->esq = e;
    a->no = n;
    return a;
}

Arv *cria_arv_huf(int *alf){
    Heap *h = criar_heap();
    Arv *a1, *a2, *raiz;
    No *n;
    ins_heap(alf,h);
    while(h->tam > 1){
        sift_down(h->tam,h);
        a1 = delete_heap(h);
        sift_down(h->tam,h);
        a2 = delete_heap(h);
//      printf("nodo esquerdo: %c  %d\n", a1->no->l, a1->no->quant);
//      printf("nodo direito: %c  %d\n", a2->no->l, a2->no->quant);
        n = (No*) malloc(sizeof(No));
        n->l = '*';
        n->quant = a1->no->quant + a2->no->quant;
//      printf("raiz: %c  %d\n", n->l, n->quant);
        raiz = cria_arv(a1, a2, n);
        build_heap(raiz, h);
    }
    return raiz;
}

void imp_preordem(Arv *raiz){
  if(raiz == NULL) return;
  printf("<%c>", raiz->no->l);
  imp_preordem(raiz->esq);
  imp_preordem(raiz->dir);
}

int gera_cod(Arv *a, int carac, char *codigo, int i){
  if(a->no->l == carac && a->esq==NULL && a->dir == NULL){
    codigo[i]='\0';
  	return 1;
  }
  else{
    int verif = 0;
        if (a->esq != NULL){
            codigo[i] = '0';
			verif = gera_cod( a->esq, carac, codigo,i + 1);
        }
        if (verif == 0 && a->dir){
            codigo[i] = '1';
            verif = gera_cod( a->dir, carac, codigo, i + 1);
        }
        if (verif == 0)  codigo[i] = '\0';
        return verif;
    }
}