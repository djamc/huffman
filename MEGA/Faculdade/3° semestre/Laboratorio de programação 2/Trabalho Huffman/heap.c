#include <stdio.h>
#include <stdlib.h>
#include "heap.h"
#include "huffman.h"

/*
 *	Anderson Madeira da Cruz
 * 	1701570240
 * 	Laboratorio de programação II
 * */

//Função para alocar espaço de memoria para a Heap
//Não recebe nenhum parametro
Heap *criar_heap(){
	Heap *h = (Heap*) malloc(sizeof(Heap));												//Aloca espaço de memoria para a struct Heap
	h->tam = 0;																										//Zera o tamanho da Heap
	h->tam_vet = 0;																								//Zera o tamanha do vetor usado pela Heap
	h->vet = NULL;																								//Aponta o ponteiro do vetor para null
	printf("Heap criado com sucesso\n");
	return h;																											//Retorna o endereço da Heap
}

Arv **cria_vet(Heap *h){
	if(h->tam_vet == 0){																					//Verifica se o tamanho do vetor for igual a 0
		h->tam_vet = 50;																						//A variavel h->tam_vet recebe o valor 50
		return ((Arv**) malloc(50 * sizeof (Arv*)));					//Aloca espaço de memoria para um vetor de ponteiro e retorna o endereço
	}
	else{																													//Caso o tamanho h->tam_vet não seja igual a 0
		int i, tam = ((h->tam / 50) + 1) * 50;											//Calcula o tam
		h->tam_vet = tam;																						//A variavel h->tam_vet recebe o valor da variavel tam
		Arv **v = (Arv**) malloc(tam * sizeof (Arv*));			//Aloca espaço de memoria para um vetor de ponteiro
		for(i = 0; i < h->tam; i++){
			v[i] = h->vet[i];																					//Copia todos os valores do vetor h->vet para o novo vetor v
		}
		free(h->vet);																								//Libera o espaço de memoria de memoria apontado pra h->vet
		return v;																										//Retorna o endereço do novo vetor
	}
}

//Função que insere um novo elemento na Heap
//Recebe como parametro o endereço da aresta que será inserida na Heap e o endereço da Heap
void build_heap(Arv *a, Heap *h){
	if(h == NULL){																										//Verifica se a Heap foi criada, caso não foi criada ele informa ao usuario e retorna
		printf("Heap não foi criado ainda\nNao e possivel inserir valor\n");
		return;
	}
	h->tam++;																													//Incrementa a variavel h->tam
	if(h->tam_vet == 0 || h->tam == h->tam_vet)	h->vet = cria_vet(h);	//Verifica se o tamanho do vetor for igual a 0 e se o tamanho da Heap for igual ao tamanho do vetor, caso sim ele chama a função para criar o vetor
	if(h->tam == 1)	h->vet[0] = a;																		//Verifica se o tamanho da heap for igual a 1
	else{																															//Caso não
		h->vet[h->tam - 1] = a;																					//Insere o endereço da nova aresta no na posição livre do vetor
		percolate((h->tam - 1), h);																			//Chama a função percolate para organizar a Heap, do fim pra o inicio
	}
}

//Função que organiza a heap
//Recebe como parametro a posição que ele começara organizar e o endereço da Heap
void percolate(int pos, Heap *h){
	/***************************Variaveis*************************/
	Arv *aux;
	/*************************************************************/
	if(pos == 0)	return;																					//Verifica se pos a ser organiada for igual a 0, caso sim ele retorna
	int pai = busca_pai(pos);																			//Chama a função buscar pai para descobrir o pai do nodo
	if(h->vet[pos]->no->quant < h->vet[pai]->no->quant){										//Verifica se o peso da aresta pos for menor do que o peso da aresta pai
		aux = h->vet[pos];																					//O ponteiro aux recebe o endereço apontado por h->vet[pos]
		h->vet[pos] = h->vet[pai];																	//O ponteiro h->vet[pos] recebe o endereço apontado por h->vet[pai]
		h->vet[pai] = aux;																					//O ponteiro h->vet[pai] recebe o endereço apontado por aux
	}
	if(h->vet[pos]->no->quant == h->vet[pai]->no->quant){
		if(h->vet[pos]->no->l < h->vet[pai]->no->l){
			aux = h->vet[pai];																					//O ponteiro a recebe o endereço o endereço apontado por h->vet[f_esq]
			h->vet[pai] = h->vet[pos];																//O ponteiro h->vet[f_esq] recebe o endereço apontado por h->vet[pos]
			h->vet[pos] = aux;																						//O ponteiro h->vet[pos] recebe o endereço apontado por a
		}
	}
	percolate( pai, h);																						//Chama a função percolate e passa a posição do pais para organizar toda a Heap
}

//Função que retira o elemento do topo da Heap
//Recebe como parametro o endereço da Heap
Arv *delete_heap(Heap *h){
	/***************************Variaveis*************************/
	Arv *a;
	/*************************************************************/
	if(h->vet == NULL)	return NULL;															//Verifica se a Heap foi criada, se for retorna null
	h->tam--;																											//Decrementa a variavel h->tam
	a = h->vet[0];																								//O ponteiro a recebe o endereço apontado pelo o primeiro emento do vetor
	h->vet[0] = h->vet[h->tam];																		//Coloca o endereço do ultimo elemento do vetor no primeiro elemento
	if(h->tam == (h->tam_vet / 2))	h->vet = cria_vet(h);					//Verifica se a veriavel tamanho da heap é igual ao tamanho do vetor / 2, caso for ele chama a função cria_vet
	sift_down( 0, h);																							//Chama a função sift_down para organizar a Heap do inicio para o fim
	return a;																											//Retorna o endereço apontado por a
}

//Função que organiza a Heap
//Recebe como parametro a posição para organizar e o endereço da Heap
void sift_down( int pos, Heap *h){
	/***************************Variaveis*************************/
	Arv *a;
	/*************************************************************/
	int f_esq = busca_filho(pos);																	//Chama a função busca filho e mandaa posição, para descobrir o filho da esquerda
	int f_dir = f_esq + 1;																				//Para saber o filho da direita é so somar um no filho da esquerda
	if( f_esq > (h->tam - 1) || f_dir > (h->tam - 1))	return;			//Verifica se filho da esquerda ou filho da direita e maior que tamanho da Heap, caso sim ele retorna
	if(h->vet[f_esq]->no->quant < h->vet[pos]->no->quant){									//Verifica se peso do filho da esquerda for menor peso da posição
		a = h->vet[f_esq];																					//O ponteiro a recebe o endereço o endereço apontado por h->vet[f_esq]
		h->vet[f_esq] = h->vet[pos];																//O ponteiro h->vet[f_esq] recebe o endereço apontado por h->vet[pos]
		h->vet[pos] = a;																						//O ponteiro h->vet[pos] recebe o endereço apontado por a
		sift_down( f_esq, h);																				//Chama a função sift_down para organizar a Heap com o filho da esquerda
	}
	if(h->vet[f_dir]->no->quant < h->vet[pos]->no->quant){									//Verifica se peso do filho da direita for menor peso da posição
		a = h->vet[f_dir];																					//O ponteiro a recebe o endereço o endereço apontado por h->vet[f_dir]
		h->vet[f_dir] = h->vet[pos];																//O ponteiro h->vet[f_dir] recebe o endereço apontado por h->vet[pos]
		h->vet[pos] = a;																						//O ponteiro h->vet[pos] recebe o endereço apontado por a
		sift_down( f_dir, h);																				//Chama a função sift_down para organizar a Heap com o filho da direita
	}
	if(h->vet[f_esq]->no->quant == h->vet[pos]->no->quant){
		if(h->vet[f_esq]->no->l < h->vet[pos]->no->l){
			a = h->vet[f_esq];																					//O ponteiro a recebe o endereço o endereço apontado por h->vet[f_esq]
			h->vet[f_esq] = h->vet[pos];																//O ponteiro h->vet[f_esq] recebe o endereço apontado por h->vet[pos]
			h->vet[pos] = a;																						//O ponteiro h->vet[pos] recebe o endereço apontado por a
			sift_down( f_esq, h);
		}
	}
	if(h->vet[f_dir]->no->quant == h->vet[pos]->no->quant){
		if(h->vet[f_dir]->no->l < h->vet[pos]->no->l){
			a = h->vet[f_dir];																					//O ponteiro a recebe o endereço o endereço apontado por h->vet[f_esq]
			h->vet[f_dir] = h->vet[pos];																//O ponteiro h->vet[f_esq] recebe o endereço apontado por h->vet[pos]
			h->vet[pos] = a;																						//O ponteiro h->vet[pos] recebe o endereço apontado por a
			sift_down( f_dir, h);
		}
	}
}

//Função para descobrir a posição do filho na Heap
//Recebe como parametro a posição que queira buscar o filho
int busca_filho(int i){
	i++;																													//Incrementa a variavel i
	i = (i * 2) -1;																								//i é multiplicado por 2 e decremntado
	return(i);																										//Retorna o valor de i
}

//Função para descobrir a posição do pai na Heap
//Recebe como parametro a posição que queira buscar o pai
int busca_pai(int i){
	i = i + 1;																										//Incrementa a variavel i
	int aux = (i/2)-1;																						//A variavel aux recebe i que é dividido por 2 e decrementado
	return(aux);																									//Retorna o valor de aux
}

//Função que imprime toda Heap
//Recebe como parametro o endereço da Heap
void imp_heap(Heap *h){
	/***************************Variaveis*************************/
	int i;
	/*************************************************************/
	if(h == NULL){																								//Verifica se a Heap foi criada,caso sim ele informa o usuario e retorna
		printf("Heap não foi criado\nNao ha nada a imprimir\n");
		return;
	}
	printf("Vetor que descreve a arvore:\n");
	for(i = 0; i < h->tam; i++){																	//For mara percorrer todo vetor
		printf("%d	",h->vet[i]->no->quant);															//Imprime o peso da do vetor da posição i
	}
	printf("\n");																									//Quebra de linha
}

//Função que elimina o espaço de memoria que foi locado para a Heap
//Recebe como parametro o endereço da Heap
void eli_heap(Heap *h){
	free(h->vet);																									//Libera o espaço de memoria apontado por h->vet
	free(h);																											//Libera o espaço de memoria apontado por h
}

//Função que pega todas as arestas do nodo e coloca na heap
//Recebe como parametro o nodo que deseja buscar as arestas, o endereço da Heap e o endereço do Grafo
/*void ins_h(int nod, Heap *h, Grafo *g){
	/***************************Variaveis*************************/
/*	Aresta *a;
	/*************************************************************/
/*	Nodo *n = busca_nodo(nod, g);																	//Chama a função busca noso para retornar o endereço do nodo desejado
	if(n->adj == NULL)			a = NULL;															//Verifica se o ponteiro n->adj é igual a NULL, caso for o ponteiro a recebe NULL
	else										a = n->adj;														//Caso não, o ponteiro a recebe o endereço n->adj
  while(a != NULL){																							//Fica no laço enquanto a for diferente de null
    build_heap(a, h);																						//Chama a função build_heap para adicionar a nova aresta na Heap
    a = a->prox;																								//O ponteiro a recebe o endereço da proxima aresta
  }
}
*/

void ins_heap(int *alf, Heap *h){
    Arv *a;
    for(int i = 0; i < ALFABETO; i++){
        if(alf[i] != 0){
            No *n = (No*) malloc(sizeof(No));
            if(i != 26)	n->l = i + 'A';
			else		n->l = ' ';
            n->quant = alf[i];
            a = cria_arv(NULL, NULL, n);
            build_heap(a,h);
        }
    }
}