#include "h_arv.h"
/**************struct heap*********************/
typedef struct heap{
	int tam;
	int tam_vet;
	Arv **vet;
}Heap;

/**********************************************/
Heap *criar_heap();

/**********************************************/
Arv **cria_vet(Heap *h);

/**********************************************/
void build_heap(Arv *a, Heap *h);

/**********************************************/
void percolate(int pos, Heap *h);

/**********************************************/
Arv *delete_heap(Heap *h);

/**********************************************/
void sift_down( int pos, Heap *h);

/**********************************************/
int busca_filho(int i);

/**********************************************/
int busca_pai(int i);

/**********************************************/
void imp_heap(Heap *h);

/**********************************************/
//void ins_h(int nod, Heap *h, Grafo *g);

/**********************************************/
void eli_heap(Heap *h);

void ins_heap(int *alf, Heap *h);