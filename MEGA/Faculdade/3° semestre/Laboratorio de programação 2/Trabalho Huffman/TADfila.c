#include <stdio.h>
#include <stdlib.h>
#include "TADfila.h"

struct fila *criafila()
{
	struct fila *f=(struct fila*)malloc(sizeof(struct fila));
	f->ini=NULL;
	f->fim=NULL;
	return f;
}

void inserefila(struct fila *f, char valor, int qtd)
{
	struct lista1 *n=(struct lista1*)malloc(sizeof(struct lista1));
	n->valor=valor;
	n->quantidade=qtd;
	n->prox=NULL;
	if(f->fim!=NULL)
	{
		f->fim->prox=n;
	}
	if(f->fim==NULL)
	{
		f->ini=n;
	}
	f->fim=n;
}

int retirafila(struct fila *f)
{
	struct lista1 *aux;
	char valor;
	if(f->ini==NULL)
	{
		printf("\nfila vazia\n");
		return 0;
	}
	aux=f->ini;
	valor=aux->valor;
	aux->quantidade--;
	f->ini=aux->prox;
	if(f->ini==NULL)
	{
		f->fim==NULL;
	}
	free(aux);
	return valor;
}

void libera(struct fila *f)
{
	struct lista1 *q=f->ini;
	while(q!=NULL)
	{
		struct lista1 *aux=q->prox;
		free(q);
		q=aux;
	}
	free(f);
}

void imprime(struct fila *f)
{
	struct lista1 *p;
	for(p=f->ini;p!=NULL;p=p->prox)
	{
			printf("<%c>",p->valor);
	}
}
