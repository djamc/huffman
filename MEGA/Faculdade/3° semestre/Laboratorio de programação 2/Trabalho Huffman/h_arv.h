typedef struct nod{
    char l;
    int quant;
}No;

typedef struct arv{
    struct arv *dir,*esq;
    No *no;
}Arv;

Arv *cria_arv(Arv *e, Arv *d, No *n);

Arv *cria_arv_huf(int *alf);

void imp_preordem(Arv *raiz);

int gera_cod(Arv *a, int carac, char *codigo, int i);